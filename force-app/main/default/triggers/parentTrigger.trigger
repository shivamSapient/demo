//Test Comment Update on 1st July
trigger parentTrigger on Parent__c (before update) {
    
    parentTriggerHandler pHandler = new parentTriggerHandler();
    if(trigger.isBefore && trigger.isInsert){
        pHandler.handleInsert(Trigger.new);
    }
    
    if(trigger.isBefore && trigger.isUpdate){
         pHandler.handleUpdate(Trigger.new);
    }
}