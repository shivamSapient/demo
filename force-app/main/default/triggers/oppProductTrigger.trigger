trigger oppProductTrigger on Product2 (after insert,after update) {
	
    List<id> productSponsored = new List<id>();
    List<id> oppIds = new List<id>();
    
    for(Product2 product : trigger.new){
        if(product.Family=='Sponsor'){
            productSponsored.add(product.id);
        }
    }
    
    List<OpportunityLineItem> oppProductList = [select id,OpportunityId from OpportunityLineItem where Product2Id in :productSponsored]; 
    
    for(OpportunityLineItem oppLineItem:oppProductList){
        oppIds.add(oppLineItem.OpportunityId);
    }
    
    List<Opportunity> oppToBeUpdated = [select id,Registration__c from Opportunity where id in :oppIds];
    
    for(Opportunity opp:oppToBeUpdated){
        opp.Registration__c = 'Sponsor';
    }
    
    if(oppToBeUpdated.size()>0){
        upsert oppToBeUpdated;
    }
}