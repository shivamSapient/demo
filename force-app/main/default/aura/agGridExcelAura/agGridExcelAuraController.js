({
    doInit : function(component, event, helper) {
        
        var rowSpanValue = 1;
        var iterationCount = 1;
        var gridOptions = {
        columnDefs: [
        {headerName: 'Document Type', field: 'documentType',
        rowSpan: function(params) {
                
                if(this.iterationCount==1)
                {
                    rowSpanValue = 3;   
                    iterationCount = iterationCount+1;                 
                }else if(iterationCount==3){
                    iterationCount = 1;
                    rowSpanValue = 1;
                }
                else{
                    iterationCount = iterationCount+1;
                    rowSpanValue = 1;
                }    
                return rowSpanValue;
            },
            cellClassRules: {
            "cell-span": "rowSpanValue=='3'"
        }},
        {headerName: 'Document Number', field: 'documentNumber', sortable: true, filter: true,
        rowSpan: function(params) {
                return rowSpanValue;
            },
            cellClassRules: {
            "cell-span": "rowSpanValue=='3'"
        }},
        {headerName: 'Date of Recording', field: 'dateOfRecording', type: ['dateColumn', 'nonEditableColumn'], width: 150, sortable: true, filter: true,
        rowSpan: function(params) {
                return rowSpanValue;
            },
            cellClassRules: {
            "cell-span": "rowSpanValue=='3'"
        } },
        {headerName: 'Date of Value', field: 'dateOfValue', type: ['dateColumn', 'nonEditableColumn'], width: 150, sortable: true, filter: true,
        rowSpan: function(params) {
                return rowSpanValue;
            },
            cellClassRules: {
            "cell-span": "rowSpanValue=='3'"
        } },
        {headerName: 'CIO Decision', field: 'CIODecision', sortable: true, filter: true,
        rowSpan: function(params) {
                return rowSpanValue;
            },
            cellClassRules: {
            "cell-span": "rowSpanValue=='3'"
        }},
        {headerName: 'Remarks', field: 'remarks', sortable: true, filter: true,
        rowSpan: function(params) {
                return rowSpanValue;
            },
            cellClassRules: {
            "cell-span": "rowSpanValue=='3'"
        }},
        {headerName: 'Owner Name', field: 'ownerName', filter: true},
        {headerName: 'Ownership ', field: 'ownership', type: 'numberColumn', filter: true},
        {headerName: 'Base Years', field: 'baseYears', type: ['dateColumn', 'nonEditableColumn'], width: 150, filter: true },        
        {headerName: 'Vesting', field: 'vesting', filter: true}
    ],

    defaultColDef: {
        width: 150,
        editable: true,
        filter: 'agTextColumnFilter'
    },

    columnTypes: {
        numberColumn: {width: 83, filter: 'agNumberColumnFilter'},
        medalColumn: {width: 100, columnGroupShow: 'open', filter: false},
        dateColumn: {
            // specify we want to use the date filter
            filter: 'agDateColumnFilter',

            // add extra parameters for the date filter
            filterParams: {
                // provide comparator function
                comparator: function(filterLocalDateAtMidnight, cellValue) {
                    // In the example application, dates are stored as dd/mm/yyyy
                    // We create a Date object for comparison against the filter date
                    var dateParts = cellValue.split('/');
                    var day = Number(dateParts[0]);
                    var month = Number(dateParts[1]) - 1;
                    var year = Number(dateParts[2]);
                    var cellDate = new Date(year, month, day);

                    // Now that both parameters are Date objects, we can compare
                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    } else if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    },

    rowData: null,
    floatingFilter: true,
    rowSelection: 'multiple',
    suppressRowTransform: true
};
    
// setup the grid after the page has finished loading
document.addEventListener('DOMContentLoaded', function() {
    var gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);

    /*agGrid.simpleHttpRequest({url: 'https://raw.githubusercontent.com/ag-grid/ag-grid/master/packages/ag-grid-docs/src/olympicWinnersSmall.json'}).then(function(data) {
        gridOptions.api.setRowData(data);
    });*/

    var rowData = [{"documentType":"Deed","documentNumber":"9980354513","dateOfRecording":"11/30/1998","dateOfValue":"11/30/1998","CIODecision":"CIO-SALE","remarks":"None","ownerName":"Carol","ownership":33,"baseYears":"11/30/1998","vesting":"Sole"},
{"documentType":"Deed","documentNumber":"9980354513","dateOfRecording":"11/30/1998","dateOfValue":"11/30/1998","CIODecision":"CIO-SALE","remarks":"None","ownerName":"Art C","ownership":33,"baseYears":"11/30/1998","vesting":"Sole"},
{"documentType":"Deed","documentNumber":"9980354513","dateOfRecording":"11/30/1998","dateOfValue":"11/30/1998","CIODecision":"CIO-SALE","remarks":"None","ownerName":"Ranier & Blenda","ownership":33,"baseYears":"11/30/1998","vesting":"Community Property"},
{"documentType":"Affidavit of Death","documentNumber":"2015-9879","dateOfRecording":"4/17/2015","dateOfValue":"6/14/2007","CIODecision":"CIO-Part","remarks":"Art Died","ownerName":"Carol","ownership":33,"baseYears":"11/30/1998","vesting":"Sole"},
{"documentType":"Affidavit of Death","documentNumber":"2015-9879","dateOfRecording":"4/17/2015","dateOfValue":"6/14/2007","CIODecision":"CIO-Part","remarks":"Art Died","ownerName":"Art C","ownership":33,"baseYears":"6/14/2007","vesting":"Deceased"},
{"documentType":"Affidavit of Death","documentNumber":"2015-9879","dateOfRecording":"4/17/2015","dateOfValue":"6/14/2007","CIODecision":"CIO-Part","remarks":"Art Died","ownerName":"Ranier & Blenda","ownership":33,"baseYears":"11/30/1998","vesting":"Community Property"},
{"documentType":"Grant Deed","documentNumber":"2015-9890","dateOfRecording":"4/17/2015","dateOfValue":"4/17/2015","CIODecision":"EXCL-JTC","remarks":"67% JT","ownerName":"Art C","ownership":33,"baseYears":"11/30/1998","vesting":"Deceased"},
{"documentType":"Grant Deed","documentNumber":"2015-9890","dateOfRecording":"4/17/2015","dateOfValue":"4/17/2015","CIODecision":"EXCL-JTC","remarks":"67% JT","ownerName":"Carol","ownership":33,"baseYears":"11/30/1998","vesting":"JT"},
{"documentType":"Grant Deed","documentNumber":"2015-9890","dateOfRecording":"4/17/2015","dateOfValue":"4/17/2015","CIODecision":"EXCL-JTC","remarks":"67% JT","ownerName":"Ranier & Blenda","ownership":33,"baseYears":"11/30/1998","vesting":"JT"},
{"documentType":"Grant Deed","documentNumber":"2015-10201","dateOfRecording":"7/16/2015","dateOfValue":"7/16/2015","CIODecision":"CIO-Part","remarks":"Out of JT","ownerName":"Art C","ownership":33,"baseYears":"11/30/1998","vesting":"Deceased"},
{"documentType":"Grant Deed","documentNumber":"2015-10201","dateOfRecording":"7/16/2015","dateOfValue":"7/16/2015","CIODecision":"CIO-Part","remarks":"Out of JT","ownerName":"Ranier & Blenda","ownership":33,"baseYears":"11/30/1998","vesting":"JT"},
{"documentType":"Grant Deed","documentNumber":"2015-10201","dateOfRecording":"7/16/2015","dateOfValue":"7/16/2015","CIODecision":"CIO-Part","remarks":"Out of JT","ownerName":"Ranier & Blenda","ownership":33,"baseYears":"7/16/2015","vesting":"JT"},
{"documentType":"Court Order","documentNumber":"2017-9090","dateOfRecording":"9/17/2017","dateOfValue":"11/30/1998","CIODecision":"EXCL-Rest","remarks":"Restores to 1998","ownerName":"Carol","ownership":33,"baseYears":"11/30/1998","vesting":"Sole"},
{"documentType":"Court Order","documentNumber":"2017-9090","dateOfRecording":"9/17/2017","dateOfValue":"11/30/1998","CIODecision":"EXCL-Rest","remarks":"Restores to 1998","ownerName":"Nathaniel Trustee","ownership":33,"baseYears":"11/30/1998","vesting":"Trust ( Art )"},
{"documentType":"Court Order","documentNumber":"2017-9090","dateOfRecording":"9/17/2017","dateOfValue":"11/30/1998","CIODecision":"EXCL-Rest","remarks":"Restores to 1998","ownerName":"Ranier & Blenda","ownership":33,"baseYears":"11/30/1998","vesting":"Community Property"}];

gridOptions.api.setRowData(rowData);
});
        
    }
})