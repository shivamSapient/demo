({
	init: function (cmp, event, helper) {
        cmp.set('v.mycolumns', [
            { label: 'Opportunity name', fieldName: 'opportunityName', type: 'text'},
            {
                label: 'Confidence',
                fieldName: 'confidence',
                editable : true,
                type: 'percent',
                cellAttributes: { iconName: { fieldName: 'trendIcon' },
                iconPosition: 'right' }
            },
            {
                label: 'Amount',
                fieldName: 'amount',
                editable : true,
                type: 'currency',
                typeAttributes: { currencyCode: 'EUR'}
            },
            { label: 'Contact Email', fieldName: 'contact',editable : true, type: 'email'},
            { label: 'Contact Phone', fieldName: 'phone', type: 'phone'}
        ]);

            cmp.set('v.mydata', [{
                    id: 'a',
                    opportunityName: 'Cloudhub',
                    confidence: 0.2,
                    amount: 25000,
                    contact: 'jrogers@cloudhub.com',
                    phone: '2352235235',
                    trendIcon: 'utility:down'
                },
                {
                    id: 'b',
                    opportunityName: 'Quip',
                    confidence: 0.78,
                    amount: 740000,
                    contact: 'quipy@quip.com',
                    phone: '2352235235',
                    trendIcon: 'utility:up'
            }]);
    },triggerError: function (cmp, event, helper) {
        cmp.set('v.errors', {
            rows: {
                b: {
                    title: 'We found 2 errors.',
                    messages: [
                        'Enter a valid amount.',
                        'Verify the email address and try again.'
                    ],
                    fieldNames: ['amount', 'contact']
                }
            },
            table: {
                title: 'Your entry cannot be saved. Fix the errors and try again.',
                messages: [
                    'Row 2 amount must be number',
                    'Row 2 email is invalid'
                ]
            }
        });
    }  
})