({
	handleApplicationEvent : function(component, event, helper) {
		alert("component2 called");
        var callDoinit = component.get('c.doInit');
        $A.enqueueAction(callDoinit);        
	},
	doInit : function(component, event, helper) {
		var action = component.get("c.searchContacts");
 		action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                var result =response.getReturnValue();
                if(result!=undefined && result!='')
                {   
                    component.set("v.wrapperList", result);
                }
               
            }else{
                console.log("error");
            }
        });
        $A.enqueueAction(action);  
	},
	viewContact : function(component, event, helper) {
   		 var contactId = event.getSource().get('v.value');
        
        component.set("v.isEdit",false);
        component.set("v.contactId",contactId);
        component.set("v.isView",true);        
    },
  	editContact : function(component, event, helper) {
        var contactId = event.getSource().get('v.value');
        
        component.set("v.isView",false);
        component.set("v.isEdit",true);
        component.set("v.contactId",contactId);        
    },
    closeModel: function(component, event, helper) {
      	component.set("v.isView",false);
        component.set("v.isEdit",false);
   },
 	saveContact : function(component, event, helper) {
        try {
		component.find("edit").get("e.recordSave").fire();
        }catch (e) {
    		console.log(e);
  			}
		var callDoinit = component.get('c.doInit');
        $A.enqueueAction(callDoinit);    
        
      	component.set("v.isView",false);
        component.set("v.isEdit",false);        
    }    
})