({
    doinit : function(component, event, helper) {
        var action = component.get("c.getConAttachments");
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                var result = response.getReturnValue();
                component.set("v.wrapperList", result);
            }
        });
        $A.enqueueAction(action);
    },
    handleClick : function(component, event, helper) {
        var lis = event.getSource().get("v.value");
        $A.get('e.lightning:openFiles').fire({
            recordIds: [lis],
        });
    },
    
    handleComponentEvent : function(component, event, helper) {
        
        var callDoinit = component.get('c.doinit');
        $A.enqueueAction(callDoinit);
    },
    
    handleDelete : function(component, event, helper) {
        var OpenModalEvent = component.getEvent("OpenModalEvent");
        OpenModalEvent.fire();
        
        var childcomponent= component.find("childComp");
        var Ids = event.getSource().get("v.value");
        childcomponent.childmethod(Ids);
    },
    
    detailPage : function(component, event, helper) {
        var detailId1 = event.currentTarget.title;
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": detailId1,
            "slideDevName": "detail"
        });
        navEvt.fire();
    },
    openDetail : function(component, event, helper) {

        var files = event.getSource().get("v.value");
                
        var appEvent = $A.get("e.c:DeleteAttachmentsEvent");
        appEvent.setParams({
            "toDeleteIds" :  files
        });
        appEvent.fire();
    },
    
    DeleteNotify : function(component, event, helper) {
        
        var init = component.get("c.doinit");
        $A.enqueueAction(init);
    },
    
    showSpinner : function(component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function(component, event, helper) {
        component.set("v.Spinner", false); 
    },
    
    closeModal : function(component, event, helper) {
;
        
        component.set("v.isOpen", false);
        console.log("In close notify");
        
    },
})