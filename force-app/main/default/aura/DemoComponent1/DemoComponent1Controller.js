({
	saveContact : function(component, event, helper) {
		var action = component.get("c.saveContactDetails");
        action.setParams({ "conObj" :  JSON.stringify(component.get("v.conInfo"))
                         });    
        
 		action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                
				var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "Contact has been inserted successfully.",
                    "type": "success"
                });
                toastEvent.fire();     
                
			var appEvent = $A.get("e.c:appEvent");
			appEvent.fire();
        	updateContactListEvent.fire();                
            }else{
                console.log("error");
            }
        });
        $A.enqueueAction(action); 		
	},
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){ 
       component.set("v.Spinner", false);
    }    
})