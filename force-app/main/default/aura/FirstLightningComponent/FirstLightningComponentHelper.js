({
    handleClick: function(component, event, helper) {
        var btnClicked = event.getSource();         // the button
        var btnMessage = btnClicked.get("v.label"); // the button's label
        component.set("v.message", btnMessage);     // update our message
        
        var updateEvent = component.getEvent("updateExpense");
        updateEvent.setParams({ "expense": "Hello" });
        updateEvent.fire();
    }
})