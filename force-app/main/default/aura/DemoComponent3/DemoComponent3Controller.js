({
	doInit : function(component, event, helper) {
		var action = component.get("c.searchContacts");
 		action.setCallback(this, function(response) {
            var state = response.getState();
            if (component.isValid() && state === "SUCCESS"){
                var resultMap =response.getReturnValue();
                if(resultMap!=undefined && resultMap!='')
                {  
                    var resultLst = JSON.parse(resultMap);    
                    component.set("v.mycolumns", resultLst[0]);
                    component.set("v.wrapperList", resultLst[1]);
                    
                    var col = component.get("v.mycolumns");
                    col.push({type: "button", typeAttributes: {
                            label: 'View',
                            name: 'View',
                            title: 'View',
                            disabled: false,
                            value: 'view',
                            iconPosition: 'left'
                        }});
                    col.push({type: "button", typeAttributes: {
                            label: 'Edit',
                            name: 'Edit',
                            title: 'Edit',
                            disabled: false,
                            value: 'edit',
                            iconPosition: 'left'
                        }});
                    component.set("v.mycolumns", col);                    
                }
               
            }else{
                console.log("error");
            }
        });
        $A.enqueueAction(action);  
        
	},
	viewRecord : function(component, event, helper) {
        var recId = event.getParam('row').Id;
        var actionName = event.getParam('action').name;
        if ( actionName == 'Edit' ) {
            var editRecordEvent = $A.get("e.force:editRecord");
            editRecordEvent.setParams({
                "recordId": recId
            });
            editRecordEvent.fire();
        } else if ( actionName == 'View') {
            var viewRecordEvent = $A.get("e.force:navigateToURL");
            viewRecordEvent.setParams({
                "url": "/" + recId
            });
            viewRecordEvent.fire();
        }
    }    
})