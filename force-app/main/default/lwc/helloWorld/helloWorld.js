import { LightningElement, track } from 'lwc';
export default class HelloWorld extends LightningElement {
    @track greeting = 'World';
    changeHandler(event) {
        this.greeting = event.target.value;
    }
    get capitalizedGreeting() {
        return `Hello ${this.greeting.toUpperCase()}!`;
    }
    currentDate = new Date().toDateString();
    name = 'Electra X4';
   description = 'A sweet bike built for comfort.';
   category = 'Mountain';
}