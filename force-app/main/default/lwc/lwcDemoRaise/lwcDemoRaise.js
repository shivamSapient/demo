import { LightningElement } from 'lwc';

export default class LwcDemoRaise extends LightningElement {

    raiseEvent(event){
        var txtInput=this.template.querySelector(".txtInput");       
        const v = txtInput.value;  
        
        const textChangeEvent = new CustomEvent('txtChange',{
            detail: {v}
        });

        //Fire Event
        this.dispatchEvent(textChangeEvent);
    }
}