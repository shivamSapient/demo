import { LightningElement, track } from 'lwc';

export default class GreetLWC extends LightningElement {

    @track greeting="Shivam!";

    greetUser(event){
        var txtInput=this.template.querySelector(".txtInput");
        // eslint-disable-next-line no-alert
        alert('txtInput:'+txtInput.value);        
        this.greeting = txtInput.value;    
        // eslint-disable-next-line no-alert
        alert('greeting:'+this.greeting);
    }
}