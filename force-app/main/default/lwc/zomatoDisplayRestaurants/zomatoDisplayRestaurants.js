/* eslint-disable no-console */
import { LightningElement, wire, track} from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

export default class ZomatoDisplayRestaurants extends LightningElement {

    @track restaurantsList = [];

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        console.log('In connectedCallBack in ZomatoDisplayRestaurants');
        registerListener('restaurantListUpdate', this.handleRestaurants, this);
    }

    disconnectedCallback() {
        console.log('In disconnectedCallback in ZomatoDisplayRestaurants');
        unregisterAllListeners(this);
    }

    handleRestaurants(restaurants) {
        console.log('In handleRestaurants in ZomatoDisplayRestaurants');
        console.log('restaurants in handle', restaurants);
        this.restaurantsList = restaurants;
        console.log('this.restaurantsList', this.restaurantsList);
    }

    handleRestaurantSelected(event) {
        this.restaurantSelected = event.target.value;
    }
}