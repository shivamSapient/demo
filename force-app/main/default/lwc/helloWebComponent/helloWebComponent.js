import { LightningElement, track } from 'lwc';
export default class HelloWebComponent extends LightningElement {
    @track greeting = 'World';
    handleGreetingChange(event) {
        this.greeting = event.target.value;
    }
    get capitalizedGreeting() {
        return `Hello ${this.greeting.toUpperCase()}!`;
    }
    currentDate = new Date().toDateString();
    name = 'Electra X4';
   description = 'A sweet bike built for comfort.';
   category = 'Mountain';
}