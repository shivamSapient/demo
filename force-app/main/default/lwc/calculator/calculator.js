import { LightningElement,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Calculator extends LightningElement {
@track firstNumber;
@track secondNumber;
@track currentResult;
@track operatorUsed;
@track finalResult

firstNumberChange(event){
    this.firstNumber = event.target.value;
}

secondNumberChange(event){
    this.secondNumber = event.target.value;
}

addNumber(){
    // eslint-disable-next-line radix
    this.currentResult = +parseInt(this.firstNumber) + +parseInt(this.secondNumber);
    this.operatorUsed = '+';

    if(this.currentResult===0 || this.currentResult){
        this.finalResult= `Result of ${this.firstNumber} ${this.operatorUsed} ${this.secondNumber} is ${this.currentResult}`;
    }else{
        this.finalResult='';
    }      
}
ShowToastMessage() {
    const toastEvnt = new  ShowToastEvent( {
          title: 'Can not divide by 0' ,
          message: this.msg ,
          variant: 'error' ,
    });
    this.dispatchEvent (toastEvnt);
}
subtractNumber(){
    // eslint-disable-next-line radix
    this.currentResult = +parseInt(this.firstNumber) - +parseInt(this.secondNumber);
    this.operatorUsed = '-';

    if(this.currentResult===0 || this.currentResult){
        this.finalResult= `Result of ${this.firstNumber} ${this.operatorUsed} ${this.secondNumber} is ${this.currentResult}`;
    }else{
        this.finalResult='';
    }  
}

multiplyNumber(){
    // eslint-disable-next-line radix
    this.currentResult = +parseInt(this.firstNumber) * +parseInt(this.secondNumber);
    this.operatorUsed = '*';

    if(this.currentResult===0 || this.currentResult){
        this.finalResult= `Result of ${this.firstNumber} ${this.operatorUsed} ${this.secondNumber} is ${this.currentResult}`;
    }else{
        this.finalResult='';
    }  
}

divideNumber(){
    // eslint-disable-next-line radix
    this.currentResult = +parseInt(this.firstNumber) / +parseInt(this.secondNumber);
    this.operatorUsed = '/';

    if(this.secondNumber==='0'){
        this.ShowToastMessage();
        this.finalResult='';
        return;
    }
    
    if(this.currentResult===0 || this.currentResult){
        this.finalResult= `Result of ${this.firstNumber} ${this.operatorUsed} ${this.secondNumber} is ${this.currentResult}`;
    }else{
        this.finalResult='';
    }  
}

}