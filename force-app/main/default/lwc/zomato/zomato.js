/* eslint-disable no-console */
import { LightningElement, track, wire} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getLocation from '@salesforce/apex/zomatoClass.getLocation';
import searchRestaurants from '@salesforce/apex/zomatoClass.searchRestaurants';
import { CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';

export default class Zomato extends LightningElement {
    location = '';
    entityId = '';
    @track selectedLocation = '';
    entityType = '';
    restaurant = '';

    @wire(CurrentPageReference) pageRef;

    handleLocationChange(event) {
        this.location = event.target.value;
    }

    handleRestaurantChange(event) {
        this.restaurant = event.target.value;
    }

    selectLocation() {

        getLocation({ 'locationName' : this.location })
        .then(result => {
            const output = JSON.parse(result);
            this.error = undefined
            console.log("Location::::", output);

            if(output !== undefined) {
                this.entityId = output[0].entity_id;
                this.entityType = output[0].entity_type;
                this.selectedLocation = output[0].title;
                console.log('this.selectedLocation',this.selectedLocation+this.entityId+this.entityType);
                
                const evt = new ShowToastEvent({
                    title: 'Success',
                    message: 'Location Search Successful',
                    variant: 'success',
                });
                this.dispatchEvent(evt);
            }
            else {
                console.log("No info returned in call back in selectLocation method");
            }
        })
        .catch(error => {
            this.message = undefined;
            this.error = error;
            console.log("error", JSON.stringify(this.error));
        });
    }

    searchRestaurant() {

        console.log('restaurant selection data',this.restaurant+''+this.entityId+''+this.entityType);

        searchRestaurants({ 'entityId' : this.entityId, 'entityType' : this.entityType, 'searchTerm' : this.restaurant})
        .then(result => {
            this.message = JSON.parse(result);
            this.error = undefined
            console.log("Restaurant List::::", this.message);

            if(this.message !== undefined) {
                console.log("searchRestaurants pubSub fires....");
                fireEvent(this.pageRef, 'restaurantListUpdate', this.message);
            }
            else {
                console.log("No info returned in call back in searchRestaurant method");
            }
        })
        .catch(error => {
            this.message = undefined;
            this.error = error;
            console.log("error", JSON.stringify(this.error));
        });
    }
}