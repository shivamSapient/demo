/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-unused-vars */
/* eslint-disable vars-on-top */
import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import gojszip from '@salesforce/resourceUrl/gojszip';
import { loadScript } from 'lightning/platformResourceLoader';

export default class ChangeOfTitleDisplay extends LightningElement {
  renderedCallback() {
        Promise.all([
            loadScript(this, gojszip + '/go.js')
          ]).then(() => {
            
            var $ = go.GraphObject.make;  // for conciseness in defining templates
            
            const myDiagramDivConst = this.template.querySelector('div.myDiagramDiv');

            var myDiagram =
              $(go.Diagram, myDiagramDivConst,
                {
                  allowCopy: false,
                  "draggingTool.dragsTree": false,
                  "commandHandler.deletesTree": true,
                  layout:
                    $(go.TreeLayout,
                      { angle: 90, arrangement: go.TreeLayout.ArrangementFixedRoots }),
                  "undoManager.isEnabled": true
                });
         
            var bluegrad = $(go.Brush, "Linear", { 0: "#99b3ff" });
            var greengrad = $(go.Brush, "Linear", { 0: "#B1E2A5"});
            
            var actionTemplate =
              $(go.Panel, "Horizontal",
                $(go.Shape,
                  { width: 12, height: 12 },
                  new go.Binding("figure"),
                  new go.Binding("fill")
                ),
                $(go.TextBlock,
                  { font: "10pt Verdana, sans-serif" },
                  new go.Binding("text")
                )
              );
        
            myDiagram.nodeTemplateMap.add("Terminal",
              $(go.Node, "Spot",
                $(go.Shape, "Circle",
                  { width: 90, height: 90, fill: "#99b3ff", stroke: null }
                ),
                $(go.TextBlock,
                  { font: "10pt Verdana, sans-serif",width: 90, wrap: go.TextBlock.WrapFit },
                  new go.Binding("text")
                ),
            $("TreeExpanderButton",
              { alignment: go.Spot.Bottom, alignmentFocus: go.Spot.Top },
              { visible: true })
              )
            );
            myDiagram.nodeTemplateMap.add("OwnerChange",
              $(go.Node, "Spot",
                $(go.Shape, "Rectangle",
                  { width: 100, height: 150, fill: "#B1E2A5", stroke: null }
                ),
                $(go.TextBlock,
                  { font: "10pt Verdana, sans-serif",width: 100, wrap: go.TextBlock.WrapFit },
                  new go.Binding("text")
                )
              )
            );      
            myDiagram.linkTemplate =
              $(go.Link, go.Link.Orthogonal,
                { deletable: false, corner: 10 },
                $(go.Shape,
                  { strokeWidth: 2 }
                ),
                $(go.TextBlock, go.Link.OrientUpright,
                  {
                    background: "white",
                    visible: false,  // unless the binding sets it to true for a non-empty string
                    segmentIndex: -2,
                    segmentOrientation: go.Link.None
                  },
                  new go.Binding("text", "answer"),
                  new go.Binding("visible", "answer", function(a) { return (a ? true : false); })
                )
              );
            var nodeDataArray = [
              { key: 1, category: "Terminal", text: "Tom owns 100%" },
              { key: 2, category: "OwnerChange", text: "Deed 98191715 DOR 11/30/1998 Remarks Sale" },
              { key: 3, category: "Terminal", text: "Carol Asuncion owns 33.33%" },
              { key: 4, category: "Terminal", text: "Art C. Asuncion owns 33.33%"},
              { key: 5, category: "Terminal", text: " Ranier & Blenda owns 33.33%" },
              { key: 6, category: "OwnerChange", text: "Deed 2015-038582 DOR 4/17/2015 Affidavit of Death / Reassessed 33% as of 6/14/2007" },
              { key: 7, category: "Terminal", text: "Carol Asuncion owns 33.33%" },
              { key: 8, category: "Terminal", text: " Ranier & Blenda owns 33.33%" },
              { key: 9, category: "OwnerChange", text: "Deed 2015-038696 DOR 4/17/2015 Carol and R & B Ilejay grant to themselves all as joint tenants" },
              { key: 10, category: "Terminal", text: "Carol Asuncion owns 33.33%" },
              { key: 11, category: "Terminal", text: " Ranier & Blenda owns 66.66%" },
              { key: 12, category: "OwnerChange", text: "Deed 2015-075243 DOR 7/16/2015 Restores ownership before the 2015 Deeds and Art's 33% goes to trust" },
              { key: 13, category: "Terminal", text: "Carol Asuncion owns 33.33%" },
              { key: 14, category: "Terminal", text: " Nathaniel Asuncion, Trustee owns 33.33%" } ,
              { key: 15, category: "Terminal", text: " Ranier & Blenda owns 33.33%" }        
            ];
            var linkDataArray = [
              { from: 1, to: 2 },
              { from: 2, to: 3},
              { from: 2, to: 4},
              { from: 2, to: 5},
              { from: 3, to: 6},
              { from: 4, to: 6},
              { from: 5, to: 6},
              { from: 6, to: 7},
              { from: 6, to: 8},
              { from: 7, to: 9},
              { from: 8, to: 9},
              { from: 9, to: 10}, 
              { from: 9, to: 11},
              { from: 10, to: 12}, 
              { from: 11, to: 12},
              { from: 12, to: 13}, 
              { from: 12, to: 14},
              { from: 12, to: 15}                
            ];
            // create the Model with the above data, and assign to the Diagram
            myDiagram.model = $(go.GraphLinksModel,
              {
                copiesArrays: true,
                copiesArrayObjects: true,
                nodeDataArray: nodeDataArray,
                linkDataArray: linkDataArray
              }); 
        })
        }
}