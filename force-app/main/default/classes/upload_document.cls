public class upload_document {
	
    public static void scan(){

        ContentVersion objPDF = [SELECT id,VersionData FROM ContentVersion WHERE ContentDocumentId = '0690K00000HHnuAQAT' 
                             AND IsLatest = true]; // Add id filter to get proper pdf attachment

        String strApiKey = '269248c137bafe1b41e411ae27d393043f963855';
        String strParserId = 'kywfbiakrlcv';
        String strTargetURL = 'https://api.docparser.com/v1/document/upload/' + strParserId + '?remote_id='+objPDF.Id;
        String strSeparationKey = 'abc';
        
        String strHeader = '--' + strSeparationKey + '\nContent-Disposition: form-data; name="file"; filename="Test2"\nContent-Type: application/octet-stream\n\n';
        String strBody = EncodingUtil.base64Encode(objPDF.VersionData);
        String strFooter = '\n--' + strSeparationKey + '--';
        
        String strHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(strHeader+'\n'));
        while(strHeaderEncoded.endsWith('=')) {
        strHeader+=' ';
        strHeaderEncoded = EncodingUtil.base64Encode(Blob.valueOf(strHeader+'\n'));
        }
        String strBodyEncoded = strBody;
        String strFooterEncoded = EncodingUtil.base64Encode(Blob.valueOf(strFooter));
        
        Blob blobBody = null;
        String last4Bytes = strBodyEncoded.substring(strBodyEncoded.length()-4,strBodyEncoded.length());
        
        if(last4Bytes.endsWith('=')) {
        Blob decoded4Bytes = EncodingUtil.base64Decode(last4Bytes);
        HttpRequest objHttpRequest = New HttpRequest();
        objHttpRequest.setBodyAsBlob(decoded4Bytes);
        String last4BytesFooter = objHttpRequest.getBody()+strFooter;
        blobBody = EncodingUtil.base64Decode(strHeaderEncoded+strBodyEncoded.substring(0,strBodyEncoded.length()-4)+EncodingUtil.base64Encode(Blob.valueOf(last4BytesFooter)));
        } else {
        blobBody = EncodingUtil.base64Decode(strHeaderEncoded+strBodyEncoded+strFooterEncoded);
        }
        
        if(blobBody.size()>3000000) {
        // throw new CustomException('File size limit is 3 MBytes');
        system.debug('File size limit is 3 MBytes');
        }else{
        system.debug('blobBody.size()'+blobBody.size());
        }
        
        HttpRequest req = New HttpRequest();
        req.setHeader('Content-Type', 'multipart/form-data; boundary=' + strSeparationKey);
        req.setHeader('Authorization', 'Basic ' + strApiKey);
        req.setMethod('POST');
        req.setEndpoint(strTargetURL);
        req.setBodyAsBlob(blobBody);
        req.setHeader('Content-Length', String.valueof(req.getBodyAsBlob().size()));
        Http http = New Http();
        HTTPResponse res = http.send(req);
        system.debug('res:'+res.getBody());        
    }
    
}