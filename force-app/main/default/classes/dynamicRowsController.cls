public with sharing class dynamicRowsController {
    
    @AuraEnabled
    public static void saveaccs(String inprec){
        
        List<Account> AccList=(List<Account>)System.JSON.deserialize(inprec, List<Account>.class);
        if(AccList.size()>0 && AccList != null){
            insert AccList;
        }
    }
    
}