public with sharing class ExpensesController {
    // STERN LECTURE ABOUT WHAT'S MISSING HERE COMING SOON
    @AuraEnabled
    public static List<Account> getExpenses() {
        return [SELECT Id, Name, CreatedDate 
                FROM Account limit 5];
    }
    
    @AuraEnabled
    public static Account saveExpense(Account expense) {
        // Perform isUpdateable() checking first, then
        upsert expense;
        return expense;
    }
}