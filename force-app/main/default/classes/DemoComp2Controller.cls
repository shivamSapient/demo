public class DemoComp2Controller {

    @AuraEnabled
    public static String searchContacts()
        {
            
            List<Contact> contacts = [Select Id,Name,Email,Phone from Contact order by createddate desc Limit 5];            
            
            List<columnsWrapper> colList = new List<columnsWrapper>();
            for(Schema.FieldSetMember fld :SObjectType.Contact.FieldSets.DisplayContactInList.getFields()) {
             	columnsWrapper col = new columnsWrapper();
                
                col.label = fld.getLabel();
                col.fieldName = fld.getFieldPath();
                col.type = fld.getType();
                colList.add(col);
            }            
            
            List<Object> responseLst = new List<Object>();
            responseLst.add(colList);
            responseLst.add(contacts);            
            return JSON.serialize(responseLst);
        }
    
    
    public class columnsWrapper{
        public string label;
        public string fieldName;
        public Schema.DisplayType type;
    }
    
}