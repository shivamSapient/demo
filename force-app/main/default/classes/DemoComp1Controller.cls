public class DemoComp1Controller {

    @AuraEnabled
    public static void saveContactDetails(String conObj){
        
        Contact con = (Contact) Json.deserialize(conObj, Contact.class);
        
        if(con!=null){
            insert con;
        }
    }    
}